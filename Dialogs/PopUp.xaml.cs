﻿using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;

namespace Raptor2
{
    /// <summary>
    /// Interaction logic for PopUp.xaml
    /// </summary>  
    /// 

    public partial class PopUp : Window
    {
        public bool isOpen = true;

        public bool isLoading
        {
            get { return Spinner.Visibility == Visibility.Visible ? true : false; }
            set { Spinner.Visibility = value ? Visibility.Visible : Visibility.Hidden; }
        }

        public PopUp(string message, double? x = null, double? y = null)
        {
            InitializeComponent();
            Message.Content = message;
            this.Loaded += (o, s) =>
            {
                this.Left = x == null ? 0 : (double)x;
                this.Top = y == null ? System.Windows.SystemParameters.WorkArea.Bottom - this.ActualHeight : (double)y;
            };
        }

        public void SetMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.Message.Content = message;
                if (!isOpen)
                    Open();
            });
        }

        public void Open()
        {
            this.Dispatcher.Invoke(() =>
            {
                isOpen = true;
                Visibility = Visibility.Visible;
                Storyboard sb = FindResource("Slide") as Storyboard;
                if (sb != null) BeginStoryboard(sb);
            });
        }

        public void ClosePopup()
        {
            this.Dispatcher.Invoke(() =>
            {
                isOpen = false;
                Storyboard sb = FindResource("Unslide") as Storyboard;
                sb.Completed += (s, ex) => { Close(); };
                if (sb != null) BeginStoryboard(sb);
                sb.Completed += delegate { this.Close(); };
            });
        }

        public void Toast(int delay = 3000)
        {
            Open();
            new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(delay);
                ClosePopup();
            })).Start();
        }

        public void PopWait(int delay = 3000)
        {
            Open();
            new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(delay);
                ClosePopup();
            })).Start();
        }

        public void SetWidth(int width)
        {
            this.Dispatcher.Invoke(delegate
            {
                Message.Width = width;
            });
        }

        public class Pop
        {
            public Pop(string message, int hangDuration = 3000, double? x = null, double? y = null)
            {
                Thread thread = new Thread(() =>
                {
                    Raptor2.PopUp pop = new Raptor2.PopUp(message, x, y);
                    pop.Toast(hangDuration);
                    pop.Closed += delegate { pop.Dispatcher.InvokeShutdown(); };
                    System.Windows.Threading.Dispatcher.Run();
                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }
    }
}