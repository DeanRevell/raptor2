﻿using Raptor2.InsightSearcher.CustomControls;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Excel = Microsoft.Office.Interop.Excel;

namespace Raptor2.Dialogs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class InsightSearchForm : Window
    {
        SQLiteConnection connection;
        SQLiteCommand command;
        SQLiteDataReader reader;

        Excel.Workbook book;
        string region;
        public Product product;

        List<List<string>> SearchResults;
        public string selectedProduct;

        public InsightSearchForm(Excel.Workbook workbook, string region)
        {
            InitializeComponent();
            book = workbook;
            this.region = region;
            connection = new SQLiteConnection(@"Data Source = C:\Easy Price Pro\PriceFile\PriceQB3.db");
            connection.Open();
            this.Closed += (e, o) => { connection.Close(); };
        }

        private void ProductSearch_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Search();
        }

        private void UpdateList(List<List<string>> results)
        {
            this.Dispatcher.Invoke(delegate
            {
                SearchProducts.Children.Clear();
            });


            foreach (List<string> product in results)
            {
                this.Dispatcher.Invoke(delegate
                {
                    ResultLabel label = new ResultLabel();
                    label.PreviewMouseDown += (o, s) => { SelectProduct(product[0]); };

                    label.ProductCodeLabel.Content = product[0];
                    label.DescriptionLabel.Content = product[1];
                    label.UnitTypeLabel.Content = product[2];
                    label.PriceLabel.Content = "£" + product[3];
                    SearchProducts.Children.Add(label);
                });
                Thread.Sleep(5);
            }
        }

        private void SelectProduct(string productCode)
        {
            List<string> searchResults = SearchInsightPriceFile(productCode, new[] { 1, 2, 3, 5, 8 }, true)[0];

            product = new Product();

            product.productCode = productCode;
            product.description = searchResults[0];
            product.qb2Price = searchResults[1];
            product.unit = searchResults[2];
            product.bmPreferPrice = searchResults[3];
            product.qb1Price = searchResults[4];
            this.DialogResult = true;
        }

        private void Search()
        {
            string text = ProductSearch.Text;

            new Thread(() =>
            {
                SearchResults = SearchInsightPriceFile(text, new[] { 0, 1, 3, 2 }, true);

                if (SearchResults.Count < 1)
                {
                    SearchResults = SearchInsightPriceFile(text, new[] { 0, 1, 3, 2 }, false);
                }

                if (SearchResults != null)
                {
                    UpdateList(SearchResults);
                }
            }).Start();
        }

        private string CreateSqlString(string search, bool searchProductCode)
        {
            if (searchProductCode)
            {
                return $"SELECT * FROM Price WHERE BackdropTerm = '{region}' AND Code = '{search}'";
            }

            string[] words = search.Split(' ');
            var sql = $"SELECT * FROM Price WHERE BackdropTerm = '{region}' AND FullDesc LIKE '%{words[0]}%'";

            for (int i = 1; i < words.Length; i++)
            {
                sql = sql + $" AND FullDesc LIKE '%{words[i]}%'";
            }
            return sql;
        }

        private List<List<string>> SearchInsightPriceFile(string search, int[] getColumns, bool isProductCode)
        {
            if (search != "")
            {
                try
                {
                    var words = search.Split(' ');
                    command = new SQLiteCommand(CreateSqlString(search, isProductCode), connection);
                    reader = command.ExecuteReader();

                    List<List<string>> results = new List<List<string>>();

                    while (reader.Read() != false)
                    {
                        List<string> match = new List<string>();

                        foreach (int column in getColumns)
                        {
                            match.Add(reader.GetString(column).ToString());
                        }
                        results.Add(match);
                    }
                    return results;
                }
                catch (Exception e) { return null; }
            }
            return null;
        }

        #region structs

        public class Product
        {
            public string productCode = null;
            public string description = null;
            public string qb2Price = null;
            public string unit = null;
            public string bmPreferPrice = null;
            public string qb1Price = null;
        }

        #endregion
    }
}
