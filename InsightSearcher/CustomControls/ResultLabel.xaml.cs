﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Raptor2.InsightSearcher.CustomControls
{
    /// <summary>
    /// Interaction logic for ResultLabel.xaml
    /// </summary>
    public partial class ResultLabel : UserControl
    {
        private bool _isHeader = false;

        public bool IsHeader
        {
            get { return _isHeader; }
            set { _isHeader = value; InitHeader(); }
        }

        public ResultLabel()
        {
            InitializeComponent();
        }

        public void InitHeader()
        {
            if (IsHeader)
            {
                Grid_MouseEnter();
                UnitTypeLabel.FontWeight = FontWeights.Bold;
                DescriptionLabel.FontWeight = FontWeights.Bold;
                PriceLabel.FontWeight = FontWeights.Bold;
                ProductCodeLabel.FontWeight = FontWeights.Bold;
                MainGrid.Cursor = Cursors.Arrow;
            }
        }

        private void Grid_MouseEnter(object sender = null, System.Windows.Input.MouseEventArgs e = null)
        {
            UnitTypeLabel.Foreground = Brushes.White;
            DescriptionLabel.Foreground = Brushes.White;
            PriceLabel.Foreground = Brushes.White;
            ProductCodeLabel.Foreground = Brushes.White;
            MainGrid.Background = new SolidColorBrush(Color.FromRgb(41, 67, 88));
        }

        private void Grid_MouseLeave(object sender = null, System.Windows.Input.MouseEventArgs e = null)
        {
            if (!IsHeader)
            {
                UnitTypeLabel.Foreground = Brushes.Black;
                DescriptionLabel.Foreground = Brushes.Black;
                PriceLabel.Foreground = Brushes.Black;
                ProductCodeLabel.Foreground = Brushes.Black;
                MainGrid.Background = Brushes.White;
            }
        }
    }
}
