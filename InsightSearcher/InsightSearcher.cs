﻿using Raptor2.Dialogs;
using static Raptor2.Dialogs.InsightSearchForm;
using Excel = Microsoft.Office.Interop.Excel;

namespace Raptor2.Tools
{
    public class InsightSearcher
    {
        private InsightSearchForm searchForm;
        Product product = new Product();

        public string productCode { get { return product.productCode; } }
        public string description { get { return product.description; } }
        public string qb2Price { get { return product.qb2Price; } }
        public string unit { get { return product.unit; } }
        public string bmPreferPrice { get { return product.bmPreferPrice; } }
        public string qb1Price { get { return product.qb1Price; } }

        public bool GetProduct(Excel.Workbook book, string region)
        {
            searchForm = searchForm == null ? new InsightSearchForm(book, region) : searchForm;
            searchForm.ProductSearch.Focus();
            searchForm.ShowDialog();
            if (searchForm.DialogResult == true)
            {
                product = searchForm.product;
                return true;
            }
            return false;
        }
    }
}